var menu = ["Bún bò", "Bún riu", 3, true, "Bún riu"];

// update 1 phần tử trong array
menu[0] = "Bánh canh";
// menu[0]  : phần tử tại index = 0
// thêm 1 phần tử vào danh sách
menu.push("Cơm tấm");
console.log(`  🚀 __ file: index.js:10 __ menu`, menu, menu.length);

// duyệt mảng
for (var i = 0; i < menu.length; i++) {
  var monAn = menu[i];
  console.log(`  🚀 __ file: index.js:13 __ monAn`, monAn);
}
// forEach ~ high order function
menu.forEach(function (item) {
  console.log("item", item);
});

// callback function

var introMonAn = function (item) {
  console.log("mon an: ", item);
};
menu.forEach(introMonAn);
// remove
console.log("before", menu);
menu.pop();
console.log("after", menu);

// CRUD create read update delete

// tìm kiếm vị trí

// "Bún riu";

var viTri;

for (let index = 0; index < menu.length; index++) {
  var monAn = menu[index];
  console.log(monAn, index);
  if (monAn == "Bánh canh") {
    viTri = index;
  }
}
console.log(`  🚀 __ file: index.js:38 __ viTri`, viTri);
// indexOf , tìmt thấy => trả về index, tìm ko thấy => trả về -1
viTri = menu.indexOf("Bánh canh");
console.log(`  🚀 __ file: index.js:48 __ viTri`, viTri);

// splice : cut  - splice( vị trí bắt đầu, số lượng)

console.log("before splice", menu);
menu.splice(1, 1);
console.log("after splice", menu);
// slice(0, 2)~  start, before end
var cloneMenu = menu.slice(0, 3);
console.log("after slice", cloneMenu);

var colorCar = ["red", "green", "blue"];

var newColorCar = colorCar.map(function (item) {
  return "toyota " + item;
});
console.log(`  🚀: newColorCar`, newColorCar);

var nums = [3, 6, 4, 2, 6, 7, 4, 6, 7, 2, 5];

var resultArr = nums.filter(function (item) {
  return item < 5;
});
console.log(`  🚀: resultArr`, resultArr);
