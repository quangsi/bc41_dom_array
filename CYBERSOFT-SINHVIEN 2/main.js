// truyền vào 1 thẻ tr bất kì,trả về điểm thẻ tr đó
function getNameFromTrTag(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[2].innerText;
}
function getScoreFromTrTag(trTag) {
  var tdList = trTag.querySelectorAll("td");
  return tdList[3].innerText * 1;
}

// show thông tin sinh viên giỏi nhất
// lấy danh sách thẻ td chứ điểm
var tdList = document.querySelectorAll(".td-scores");
var trList = document.querySelectorAll("#tblBody tr");
console.log(`  🚀: trList`, trList);
var scoreArr = Array.from(tdList).map(function (tdTag) {
  return tdTag.innerText * 1;
});

var scoreMax = scoreArr[0];

for (var index = 1; index < scoreArr.length; index++) {
  var current = scoreArr[index];
  if (current > scoreMax) {
    scoreMax = current;
  }
}
console.log(`  🚀: scoreArr`, scoreArr);
console.log(`  🚀: scoreMax`, scoreMax);

var indexMax = scoreArr.indexOf(scoreMax);
// show thông tin
var trMax = trList[indexMax];

document.getElementById("svGioiNhat").innerText = `
 ${getNameFromTrTag(trMax)} - ${getScoreFromTrTag(trMax)}`;
// show so luong sinh vien gioi
var sinhVienGioiArr = [];
var listSinhVienTrungBinhString = "";

for (var index = 0; index < trList.length; index++) {
  // trTag: thẻ tr trong mỗi lần lặp
  let trTag = trList[index];

  if (getScoreFromTrTag(trTag) >= 8) {
    sinhVienGioiArr.push(trTag);
  }
  if (getScoreFromTrTag(trTag) >= 5) {
    var content = `<p class="text-primary">  ${getNameFromTrTag(
      trTag
    )}  - ${getScoreFromTrTag(trTag)} </p>`;

    listSinhVienTrungBinhString += content;
  }
}

document.getElementById("soSVGioi").innerText = sinhVienGioiArr.length;

document.getElementById("dsDiemHon5").innerHTML = listSinhVienTrungBinhString;

// selection , bubble
