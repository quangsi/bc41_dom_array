var titleEl = document.querySelector("footer .title");
// querySelector chỉ trả về 1 element đầu tiên khi có nhiều element hợp lệ

titleEl.style.color = "red";

var titleList = document.querySelectorAll(".title");
// querySelectorAll trả về danh sách
console.log(`  🚀 __ file: index.js:7 __ titleList`, titleList);

titleList[0].style.color = "green";
// titleList : danh sách
// titleList[0] : 1 phần tử trong danh sách với index =0
// index là vị trí, bắt đầu từ 0

console.log(titleList.length); //3
// for

for (var i = 0; i <= titleList.length; i++) {
  console.log("[i]", i);
  titleList[i].style.color = "blue";
}

// dc 1
// ko dc
